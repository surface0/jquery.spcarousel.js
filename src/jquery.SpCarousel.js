/*!
 * @fileOverview
 * @name SpCarousel.js
 * @description スワイプ対応カルーセル for スマホ jQueryプラグイン
 * @version 2.0
 * @require jQuery 1.7 or later
 * @see readme.txt
 * @author surface0
 * @license <a href="http://en.wikipedia.org/wiki/MIT_License">X11/MIT License</a>
 * @url https://bitbucket.org/surface0/spcarousel.js
 */
(function($, window, Math){
    // Custom Events
    var CE_SLIDE_START = 'SpCarousel_SlideStart',
        CE_SLIDE_END = 'SpCarousel_SlideEnd';
    
    var defaults = {
        classPrefix: 'spCarousel',
        eventNameSpace: 'spCarousel',
        
        interval: 2000,         // Number of ms
        duration: 600,          // Number of ms
        
        width: 'auto',          // 'auto' or Number of px
        height: 'auto',         // 'auto' of Number of px
        
        enableSideArrow: true,
        arrowLeft: '<img class="spCarousel_LeftArrow" src="images/SpCarousel/left.png" style="position:absolute;left:4px;top:50%;margin-top:-16px;">',
        arrowRight: '<img class="spCarousel_RightArrow" src="images/SpCarousel/right.png" style="position:absolute;right:4px;top:50%;margin-top:-16px;">',
        
        enableNavBall: true,
        navBallColor: '#666',
        navBallActiveColor: '#F9C',
        
        slideBefore: $.noop,
        slideAfter: $.noop,
        
        enableSwipe: true,
        swipeThreshold: 0.1,    // Number of threshold
        vThrehshold: 10,        // Number of threshold
        
        autoStart: true,
    };
    
    // メイン処理
    $.fn.SpCarousel = function(config){
        if (config === 'stop') {
            var instance = this.data('SpCarousel');
            instance._autoSlide = false;
            instance.stopAutoSlide();
            return this;
        }
        
        if (config === 'start') {
            var instance = this.data('SpCarousel');
            instance._autoSlide = true;
            instance.startSlide();
            return this;
        }
        
        if (config === 'next') {
            //
        }
        
        if (config === 'preview') {
            //
        }
        
        if (config === 'move') {
            //
        }
        
        return this.each(function(){
            var $this = $(this);
            $this.data('SpCarousel', new SpCarousel($this, config));
        });
    };
    
    function SpCarousel(target, config) {
        this._$instance = target;
        this._options = $.extend({}, defaults, config);
        this._currentIndex = 0;
        this._listNum = 0;
        this._timeOut = null;
        this._isSliding = false;
        this._autoSlide = this._options.autoStart;
        
        this.init();
        
        this.startSlide();
    }
    
    var p = SpCarousel.prototype;
    
    p.moveList = function(x, duration, callback) {
        var m = new WebKitCSSMatrix(this._$instance.css('WebkitTransform'));
        
        this._$instance
            .one(this.getEventName('webkitTransitionEnd'), callback)
            .css({
                WebkitTransition: duration + 'ms',
                WebkitTransform: 'translate3d(' + x + 'px, 0, 0)',
            });

        if (duration === 0 || m.e === x) {
            this._$instance.trigger('webkitTransitionEnd');
        }
    };
    
    p.slideList = function(index, duration) {
        if (!this._isSliding) {
            this._options.slideBefore();
        }
        
        this._isSliding = true;
        var x = -(this._options.width * (index + 1));
        var startParams = {
            next: index < 0 ? (this._listNum - 1) : index
        };
        
        if (duration > 0) {
            this._$instance.trigger(CE_SLIDE_START, startParams);
        }

        this.moveList(x, duration, this._onSlideEnd.bind(this));
    };
    
    p._onSlideEnd = function() {
        if (this._currentIndex >= this._listNum) {
            this._currentIndex = 0;
            this.slideList(this._currentIndex, 0);
        } else if (this._currentIndex < 0) {
            this._currentIndex = this._listNum - 1;
            this.slideList(this._currentIndex, 0);
        } else {
            this._options.slideAfter();
            this._$instance.trigger(CE_SLIDE_END, { current: this._currentIndex });
        }
        this._isSliding = false;
    };
    
    p.slideNext = function(duration) {
        this.slideList(++this._currentIndex, duration);
    };
    
    p.slidePrev = function(duration) {
        this.slideList(--this._currentIndex, duration);
    };
    
    p.startSlide = function() {
        if (this._listNum < 2) return;
        
        this._$instance
            .on(CE_SLIDE_START, this.stopAutoSlide.bind(this))//手動で動かした場合も考慮してストップかける
            .on(CE_SLIDE_END, this.startAutoSlide.bind(this));
        this.slideList(0, 0);
    };
    
    p.startAutoSlide = function() {
        if (this._autoSlide && this._timeOut === null && this._options.interval > 0) {
            var self = this;
            this._timeOut = window.setTimeout(function() { self.slideNext(self._options.duration); }, self._options.interval);
        }
    };

    p.stopAutoSlide = function() {
        if (this._timeOut !== null) {
            clearTimeout(this._timeOut);
            this._timeOut = null;
        }
    };
    
    // 左右の矢印実装
    p.attachSideArrows = function($element) {
        var $left = $(this._options.arrowLeft),
            $right = $(this._options.arrowRight);
        
        var self = this;
        var clickEventName = this.getEventName('click');
        this._$instance
            .on(CE_SLIDE_END, function() {
                $left.one(clickEventName, function() { self.slidePrev(self._options.duration); });
                $right.one(clickEventName, function() { self.slideNext(self._options.duration); });
            })
            .on(CE_SLIDE_START, function() {
                $left.off(clickEventName);
                $right.off(clickEventName);
            });
        $element.append($left, $right);
    };
    
    // 下部の球ナビ
    p.attachNavBalls = function($viewport) {
        var $navBallList = $('<ul />')
            .addClass(this.getClassName('navBall'))
            .css({
                listStyle: 'none',
                textAlign: 'center',
                margin: '0 auto',
                width: $viewport.width(),
                padding: '8px 0',
            });
        
        
        var navBalls = [];
        for (var i = 0; i < this._listNum; i++) {
            var $aElm = $('<span />', {'data-indexnum': i})
                .css({
                    display: 'block',
                    width: 12,
                    height: 12,
                    textIndent: '-9999px',
                    overflow: 'hidden',
                    borderRadius: 12,
                    backgroundColor: (i === 0 ? this._options.navBallActiveColor : this._options.navBallColor),
                });
            navBalls.push($('<li />')
                .css({
                    display: 'inline-block',
                    padding: 4,
                    margin: '0 6px',
                }).append($aElm));
            
        }
        $navBallList.append(navBalls);
        
        var self = this;
        $navBallList.find('li').on(this.getEventName('click'), function(e) {
            var idx = parseInt($(this).find('> span').data('indexnum'), 10);
            if (self._currentIndex !== idx) {
                self._currentIndex = idx;
                self.slideList(self._currentIndex, self._options.duration);
            }
        });
        
        var self = this;
        this._$instance.on(CE_SLIDE_START, function(e, param) {
            var next = Math.max(0, param.next % self._listNum);
            $navBallList.find('li > span')
                .css({backgroundColor: self._options.navBallColor})
                .filter('[data-indexnum="' + next + '"]')
                    .css({backgroundColor: self._options.navBallActiveColor});
        });
        $viewport.after($navBallList);
    };
    
    p.attachSwipe = function($viewport) {
        var dx = null, sy = null, sx = null;
        var left = 0;
        var _SWIPE_THRESOULD = this._options.width * this._options.swipeThreshold;
        var self = this;
        
        var onTouchStart = function(e) {
            if (self._isSliding) return;

            self.stopAutoSlide();
            
            left = -self._options.width * (self._currentIndex + 1);
            sx = e.originalEvent.touches[0].screenX;
            sy = e.originalEvent.touches[0].screenY;
        };
        var onTouchMove = function(e) {
            if (self._isSliding || sx === null) return;
            
            dx = e.originalEvent.touches[0].screenX - sx;
            var dy = e.originalEvent.touches[0].screenY - sy;

            if (Math.abs(dy) < self._options.vThrehshold) {
                e.preventDefault();
                var min = -self._options.width * (self._currentIndex + 2);
                var max = -self._options.width * (self._currentIndex);
                var x = Math.max(min, Math.min(max, left + dx));
                self.moveList(x, 0, $.noop);
            }
        };
        var onTouchEnd = function(e) {
            if (self._isSliding) return;
            
            if (Math.abs(dx) > _SWIPE_THRESOULD) {
                dx > 0 ? self.slidePrev(100) : self.slideNext(100);
            } else {
                self.moveList(left, 100, self.startAutoSlide.bind(self));
            }
            
            sx = dx = null;
        };
        $viewport
            .on(this.getEventName('touchstart'), onTouchStart)
            .on(this.getEventName('touchmove'), onTouchMove)
            .on(this.getEventName('touchend'), onTouchEnd);
    };
    
    p.getClassName = function(name) {
    return this._options.classPrefix + '_' + name;
    };
    
    p.getEventName = function(name) {
        return name + '.' + this._options.eventNameSpace;
    };
    
    p.init = function() {
        var $li = this._$instance.find('> li');
        if ((this._listNum = $li.length) < 2) return;

        //auto時に中身のサイズを測るためとりあえずこれだけ適用しておく
        $li.css({
            display: 'block',
            'float': 'left',
        });
        
        if (this._options.width === 'auto') {
            var widths = [];
            $li.each(function() { widths.push($(this).width()); });
            this._options.width = Math.max.apply(null, widths);
        }
        
        if (this._options.height === 'auto') {
            var heights = [];
            $li.each(function() { heights.push($(this).height()); });
            this._options.height = Math.max.apply(null, heights);
        }

        $li.css({
            width: this._options.width,
            overflow: 'hidden',
            'float': 'none',
        });
        
        //可視部分の窓を作る
        var $viewport = $('<div />')
            .addClass(this.getClassName('ViewPort'))
            .css({
                position: 'relative',//デフォルトの矢印ナビ用
                width: this._options.width,
                height: this._options.height,
                margin: '0 auto',
                overflow: 'hidden',
            });
        
        //スライドを滑らかに見せるため、複製した最初と最後の要素をダミーとして入れる
        var $first = $li.filter(':first-child').clone(),
            $last = $li.filter(':last-child').clone();
        
        //元要素をviewportで包み込む
        this._$instance
            .prepend($last)
            .append($first)
            .css({
                    display: '-webkit-box',
                    WebkitBoxPack: 'center',
                    listStyle: 'none',
                    margin: 0,
                    padding: 0,
                    width: (this._options.width * (this._listNum + 2)),
                })
            .replaceWith($viewport)
            .appendTo($viewport);

        if (this._options.enableSideArrow) this.attachSideArrows($viewport);
        if (this._options.enableNavBall) this.attachNavBalls($viewport);
        if (this._options.enableSwipe) this.attachSwipe($viewport);
    };
})(jQuery, window, Math);